<?php
class Advertising_IndexController extends Zend_Controller_Action {
	protected $tr;
	public function init()
    {    	
     /* Initialize action controller here */
    	header('content-type: text/html; charset=utf8');
    	defined('BASE_URL')	|| define('BASE_URL', Zend_Controller_Front::getInstance()->getBaseUrl());
    	$this->tr = Application_Form_FrmLanguages::getCurrentlanguage();
	}
	public function indexAction(){
		try{
			$db = new Advertising_Model_DbTable_Dbadvertising();
		    if($this->getRequest()->isPost()){
 				$search = $this->getRequest()->getPost();
 			}
			else{
				$search= array(
						'txt_search' => '',
						'province_id'=>-1,
						'company_type_search'=>0,
						'product_search'=>0,
						);
			}
			$rs_rows= $db->getAllCompany($search);
			$this->view->row = $rs_rows;
			$glClass = new Application_Model_GlobalClass();
			$rs_rows = $glClass->getImgActive($rs_rows, BASE_URL, true);
			$list = new Application_Form_Frmtable();
			$collumns = array("Company Name","PHONE","EMAIL","City_Province","STATUS","BY_USER");
			$link_info=array('module'=>'advertising','controller'=>'index','action'=>'edit',);
			$this->view->list=$list->getCheckList(0, $collumns, $rs_rows,array('com_code'=>$link_info,'com_name'=>$link_info),0);
			
		}catch (Exception $e){
			Application_Form_FrmMessage::message("Application Error");
			Application_Model_DbTable_DbUserLog::writeMessageError($e->getMessage());
		}	
	$this->view->rssearch = $search;
	$db = new Application_Model_DbTable_DbGlobal();
  	$this->view->rsprovince = $db->getAllProvince();
  	
  	$fm = new Advertising_Form_Frmadvertising();
  	$frm = $fm->FrmAddCompany();
  	Application_Model_Decorator::removeAllDecorator($frm);
  	$this->view->frm_company = $frm;
  }
  public function addAction(){
  	try{
  		$db = new Advertising_Form_Frmadvertising();
  		if($this->getRequest()->isPost()){
  			$_data = $this->getRequest()->getPost();
  			$db->addadvertising($_data);
  			if(!empty($_data['save_close'])){
  				$this->_redirect("/advertising/index");
  			}else{
  				$this->_redirect("/advertising/index/add");
  			}
  		}
  	}catch (Exception $e){
  		Application_Form_FrmMessage::message("Application Error");
  		Application_Model_DbTable_DbUserLog::writeMessageError($e->getMessage());
  	}
  	
  	$fm = new Advertising_Form_Frmadvertising();
  	$frm = $fm->FrmAddCompany();
  	Application_Model_Decorator::removeAllDecorator($frm);
  	$this->view->frm_company = $frm;
  	
  }
  public function editAction(){
  	$db = new Advertising_Model_DbTable_Dbadvertising();
  	$id = $this->getRequest()->getParam('id');
  	try{
  		if($this->getRequest()->isPost()){
  			$_data = $this->getRequest()->getPost();
  			$db->editCompany($_data);
  			$this->_redirect("/advertising/index");
  		}
  	}catch (Exception $e){
  		Application_Form_FrmMessage::message("Application Error");
  		Application_Model_DbTable_DbUserLog::writeMessageError($e->getMessage());
  	}
  	$result =  $db->getCompanyById($id);
  	if(empty($result)){Application_Form_FrmMessage::Sucessfull("NO_RECORD", "/advertising/index");}
  	$this->view->rs = $result;
  	
  	$fm = new Advertising_Form_Frmadvertising();
  	$frm = $fm->FrmAddCompany($result);
  	Application_Model_Decorator::removeAllDecorator($frm);
  	$this->view->frm_company = $frm;
  	
  	
  }
  
	
}

