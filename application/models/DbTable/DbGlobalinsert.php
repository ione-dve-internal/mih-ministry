<?php

class Application_Model_DbTable_DbGlobalinsert extends Zend_Db_Table_Abstract
{
    // set name value
	public function setName($name){
		$this->_name=$name;
	}
	public function init()
	{
		$this->tr = Application_Form_FrmLanguages::getCurrentlanguage();
	}
	public function getUserId(){
		$session_user=new Zend_Session_Namespace('auth');
		return $session_user->user_id;
	
	}
	public function insertSerViceProgramType($_data){
		$this->_name='rms_program_type';
		$_db = new Application_Model_DbTable_DbGlobal();
		$_rs = $_db->getServicTypeByName($_data['servicetype_title'],$_data['ser_type']);
		if(!empty($_rs)){
			return -1;	
		}else{
			$_arr = array(
					'title'=>$_data['servicetype_title'],
					'item_desc'=>$_data['item_desc'],
					'status'=>$_data['sertype_status'],
					'type'=>$_data['ser_type'],
					'create_date'=> new Zend_Date(),
					'user_id' => $this->getUserId()
			);
			return $this->insert($_arr);
		}
	}
	function ContactFormSendmail($data){
	
		$db = $this->getAdapter();
		$email = empty($data["email"])?'':$data["email"];
		$from_email =$email;
		$to_Email       = $data['website_email']; //receiver mail
	
// 		$key = new Application_Model_DbTable_DbKeycode();
// 		$keydata=$key->getKeyCodeMiniInv(TRUE);
		//$company_name = empty($keydata["client_company_name"])?"":$keydata["client_company_name"];
		
		$subject        = empty($data["topic_inquiry"])?'':$data["topic_inquiry"]; //Subject line for emails sender name or subject
		$messsage        = empty($data["inquiry"])?'':$data["inquiry"];//commnet or suggestion of sender
		$phone='';
		$content ='<h3>'.$subject.'</h3><br />
		<p>'.$messsage.'</p>';
		$userconten='<p>
		We will try to get back to you as early as possible.<br />
		Please note that pricing requests may take longer depending on doctor availability an case complexity.</p>';
		$subjectuser= ' Thanks for sent an Expression';
	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
		$headers .= 'From: '.$to_Email."\r\n".
				'Reply-To: '.$from_email."\r\n" .
				'X-Mailer: PHP/' . phpversion();
		 
		$headersuser  = 'MIME-Version: 1.0' . "\r\n";
		$headersuser  .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
		$headersuser  .= 'From: '.$from_email."\r\n".
				'Reply-To: '.$to_Email."\r\n" .
				'X-Mailer: PHP/' . phpversion();
	
		$sentMail = @mail($to_Email, $subject, $content, $headersuser);
		$sentMail = @mail($from_email, $subjectuser, $userconten, $headers);
	
		if(!$sentMail)  {
			$return="HTTP/1.1 500 Could not send mail! Sorry..";
			$return_s = 2;
		} else {
			$return='Hi Thank you for your email! <br/>';
			$return.='Your email has been delivered.';
			$return_s = 1;
		}
		return $return_s;
	}


	
}
?>